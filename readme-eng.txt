************************************************************************************
Plugin JRTicketPlugin.jar for custom ticket design which base on JasperReports5.2.0.
************************************************************************************


Required QSystem 17.1 or higher.

For edit template of ticket will be better to use editor iReport Designer. This is free for using.
Printer properties you can change in config/welcome.properties.
If plugin printing failed then will be use default ticket printing and design.

Templates to place in this folder:
%QSYSTEM_HOME%/plugins/jrt_template/ticket.jasper - this is template of cliet ticket
%QSYSTEM_HOME%/plugins/jrt_template/ticket_adv.jasper - this is template for pre-registration

Plugin JRTicketPlugin.jar can print some data about client. It use fields(Fields) for that. Values of those fields obtain as $F{field_name}. For ex: $F{service_name} mean the name of service, which client chooses on welcome point and so on. �� ������� ��������� ������������ � �.�. List of available field names below:

Some data(data from config/welcome.properties):
promo_text
bottom_text

Data of client:
customer_number
customer_prefix
customer_priority
customer_input_data
customer_id
service_description
service_input_caption
service_name
service_pre_info_text
service_ticket_text
service (this is class QService)
customer (this is class QCustomer)

Data of pre-registred client:
adv_customer_input_data
adv_customer_id
adv_customer
adv_customer_time
service_description
service_input_caption
service_name
service_pre_info_text
service_ticket_text
service (this is class QService)