Плагин для создания произвольного дизайна талонов на основе шаблона JasperReports5.2.0.
Требуется версия QSystem19.1 или новее.
Аналогичный плагин разработан сторонним разработчиком и опубликован тут http://qsystem.info/index.php/forum/6-%D0%9F%D0%BB%D0%B0%D0%B3%D0%B8%D0%BD%D1%8B-%D0%B2%D0%B7%D0%B0%D0%B8%D0%BC%D0%BE%D0%B4%D0%B5%D0%B9%D1%81%D1%82%D0%B2%D0%B8%D0%B5-%D1%81-%D0%B4%D1%80%D1%83%D0%B3%D0%B8%D0%BC%D0%B8-%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0%D0%BC%D0%B8/396-konstruktor-talonov
Там хорошо описаны некоторые технические моменты работы и дезайна талонов. Так же есть краткий мануал как работать с редактором iReport.

Для редактирования шаблонов лучше использовать программу iReport Designer. Она бесплатная.
На работу плагина так же влияют параметра принтера из welcome.properties.
Если во время работы плагина произойдет какая то ошибка, печать талона будет передана стандартному механизму QSystem, поэтому клиент без талона не останется.

Шаблоны располагать тут:
%QSYSTEM_HOME%/plugins/jrt_template/ticket.jasper - это шаблон талона посетителя
%QSYSTEM_HOME%/plugins/jrt_template/ticket_adv.jasper - это шаблон талона предварительной регистрации

Плагин, для передачи каких то значений, использует поля (Fields), значение из которых получаются в виде $F{имя_поля}, например $F{service_name} вернет название услуги, на которую записался пользователь и т.п. Список всех полей и их значения приведены ниже.

Параметры для использования:
promo_text
bottom_text

ѕараметры простого посетителя:
customer_number
customer_prefix
customer_priority
customer_input_data
customer_id
service_description
service_input_caption
service_name
service_pre_info_text
service_ticket_text
service (это класс QService)
customer (это класс QCustomer)

Параметры предварительно зарегистрированного посетителя:
adv_customer_input_data
adv_customer_id
adv_customer
adv_customer_time
service_description
service_input_caption
service_name
service_pre_info_text
service_ticket_text
service (это класс QService)